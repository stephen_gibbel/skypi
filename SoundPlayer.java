/**
 * Import some useful stuff...
 */
import javax.sound.sampled.*;

/** 
 * Plays a given waveform buffer.
 *
 * @author Stephen Gibbel
 */
public class SoundPlayer {
	byte[] waveformBuffer;
	int sampleRate;

	/**
	 * Constructor.
	 *
	 * @param sampleRate 		The soundcard sample rate to use.
	 * @param newWaveformBuffer	The waveform buffer to play.
	 */
	public SoundPlayer(int newSampleRate, byte[] newWaveformBuffer) {
		sampleRate = newSampleRate;
		waveformBuffer = newWaveformBuffer;
	}

	/**
	 * Plays the buffer given on construction, blocks until complete. Takes no arguments and returns nothing.
	 */
	public void playSound() throws LineUnavailableException {
		SourceDataLine audioLine;
		AudioFormat format = new AudioFormat(sampleRate, 16, 1, true, true);
		DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);

		audioLine = (SourceDataLine)AudioSystem.getLine(info);
		audioLine.open(format);
		audioLine.start();

		//Write the waveform buffer. Note that this will block until complete, should probably be threaded in the future.
		audioLine.write(waveformBuffer, 0, waveformBuffer.length);

		//Clear and dispose of the audio device
		audioLine.drain();
		audioLine.close();
	}
}