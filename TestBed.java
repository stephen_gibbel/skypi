import java.nio.ByteBuffer;
import javax.sound.sampled.*;

public class TestBed {
	public static void main(String[] args) throws LineUnavailableException {
		SourceDataLine line;
		AudioFormat format = new AudioFormat(44100, 16, 1, true, true);
		DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);

		line = (SourceDataLine)AudioSystem.getLine(info);
		line.open(format);  
		line.start();
		System.out.println(line.getBufferSize());
	}
}